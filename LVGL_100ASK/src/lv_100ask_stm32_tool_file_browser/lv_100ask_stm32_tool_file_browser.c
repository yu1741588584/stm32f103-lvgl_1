/**
 ******************************************************************************
 * @file    lv_100ask_stm32_tool_file_browser.c
 * @author  百问科技
 * @version V1.2
 * @date    2020-12-12
 * @brief	文件浏览器
 ******************************************************************************
 * Change Logs:
 * Date           Author          Notes
 * 2020-12-12     zhouyuebiao     First version
 * 2021-01-25     zhouyuebiao     V1.2 
 ******************************************************************************
 * @attention
 *
 * Copyright (C) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 * All rights reserved
 *
 ******************************************************************************
 */
 
/*********************
 *      INCLUDES
 *********************/
#include <stdlib.h>
#include <stdio.h>
#include "ff.h"
#include "lv_100ask_stm32_tool_file_browser.h"

/**********************
 *  STATIC VARIABLES
 **********************/
static PT_lv_100ask_file_browser g_pt_lv_100ask_file_browser;  // 数据结构体

/**********************
 *  STATIC PROTOTYPES
 **********************/
static bool lv_100ask_stm32_tool_file_browser_init(void);							// 界面初始化
static void lv_100ask_stm32_tool_browser_scan_dir(char * path);						// 扫描指定目录
static UINT lv_100ask_stm32_tool_file_browser_strn(const char *p, const char chr);	// 计算某个字符串出现的最后位置
static void lv_100ask_stm32_file_browser_BIT_fault(void);							// 自检失败，提示
static void event_handler_BIT_fault_back_to_home(lv_obj_t * obj, lv_event_t event);	// 自检失败，返回桌面处理函数
static void event_handler_file_browser_open_dir(lv_obj_t * obj, lv_event_t e);		// 打开目录事件处理函数
static void event_handler_file_browser_init(lv_obj_t * obj, lv_event_t event);		// 打开目录列表事件处理函数
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event);			// 返回桌面事件处理函数


	
/*
 *  函数名：   void lv_100ask_stm32_tool_file_browser(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 应用初始化入口
*/
void lv_100ask_stm32_tool_file_browser(void)
{
	g_pt_lv_100ask_file_browser = (T_lv_100ask_file_browser *)malloc(sizeof(T_lv_100ask_file_browser));  // 申请内存
	
	g_pt_lv_100ask_file_browser->bg_file_browser = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(g_pt_lv_100ask_file_browser->bg_file_browser, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_y(g_pt_lv_100ask_file_browser->bg_file_browser, 0);

	if (lv_100ask_stm32_tool_file_browser_init())
	{
		add_title(g_pt_lv_100ask_file_browser->bg_file_browser, "FILE");
		add_back(g_pt_lv_100ask_file_browser->bg_file_browser, event_handler_back_to_home);   // 返回桌面按钮
	}
}


/*
 *  函数名：   static void lv_100ask_stm32_tool_music_player_init(void)
 *  输入参数： 无
 *  返回值：   true-初始化成功 false-初始化失败
 *  函数作用： 应用界面初始化
*/
static bool lv_100ask_stm32_tool_file_browser_init(void)
{
	FATFS   fs;
	FATFS *pfs;
	DWORD fre_clust, fre_sect, tot_sect;
	FRESULT res_flash;

	pfs=&fs;//指向fs
	
	/* 挂载文件系统 */
	res_flash = f_mount(&fs, "0:", 1);    /* Mount a logical drive */
	if(res_flash == FR_OK)
	{
		printf("FATFS mount succeed!\r\n");
	}
	else if(res_flash == FR_NO_FILESYSTEM)
	{
		printf("SD not inited!!!\r\n");
		lv_100ask_stm32_file_browser_BIT_fault();
		return false;
	}
	else
	{
		printf("FATFS mount Failed!!!\r\n");
		lv_100ask_stm32_file_browser_BIT_fault();
		return false;
	}
	
	// 获取SD卡总的空间大小以及剩余空间大小
	// 计算方法：
	//   总空间大小(MB)   = 最大簇数×每簇的扇区数÷2÷1024
	//   剩余空间大小(MB) = 空闲簇数×每簇的扇区数÷2÷1024
	res_flash = f_getfree("/", &fre_clust, &pfs);         				// 空簇大小
	tot_sect = (DWORD)(pfs->n_fatent - 2) * (pfs->csize) / 2 / 1024;    // 扇区总个数
	fre_sect = fre_clust * (pfs->csize) / 2 / 1024;                 	// 空扇区个数
	//printf("r\n设备总空间：%d MB\r\n可用空间：%d MB\r\n", tot_sect, fre_sect);
	
	lv_obj_t * btn_all_storage = lv_btn_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
    lv_obj_set_size(btn_all_storage, LV_HOR_RES/3*2, 20);
    lv_obj_align(btn_all_storage, NULL, LV_ALIGN_IN_TOP_LEFT, 20, 100);
    lv_obj_set_style_local_radius(btn_all_storage, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0); // 设置圆角
    lv_obj_set_style_local_bg_color(btn_all_storage, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE); //设置颜色
    lv_obj_set_event_cb(btn_all_storage, event_handler_file_browser_init);

    lv_obj_t * btn_storage_usage = lv_btn_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
    lv_obj_set_size(btn_storage_usage, lv_obj_get_width(btn_all_storage) * (((float)tot_sect - (float)fre_sect) / (float)tot_sect), 20);
    lv_obj_set_style_local_radius(btn_storage_usage, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, 0); // 设置圆角
    lv_obj_set_style_local_bg_color(btn_storage_usage, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLUE); //设置颜色
    lv_obj_align(btn_storage_usage, NULL, LV_ALIGN_IN_TOP_LEFT, 20, 100);
    lv_obj_set_event_cb(btn_storage_usage, event_handler_file_browser_init);
	
	lv_obj_t * label;
	label = lv_label_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
	lv_label_set_text(label, LV_SYMBOL_DRIVE"SD card(0:)");
	lv_obj_align(label, btn_all_storage, LV_ALIGN_OUT_TOP_LEFT, 0, 0);
	
	label = lv_label_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
	lv_label_set_text_fmt(label, "%d MB free total %d MB", fre_sect, tot_sect);  
	lv_obj_align(label, btn_all_storage, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);
	
	return true;
}



/*
 *  函数名：  static void lv_100ask_stm32_music_BIT_fault(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：无sd卡或sd卡路径无效，退出文件浏览器
*/
static void lv_100ask_stm32_file_browser_BIT_fault(void)
{	
	lv_obj_t * label_message = lv_label_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
	lv_label_set_long_mode(label_message, LV_LABEL_LONG_BREAK);     /*Break the long lines*/
	lv_label_set_recolor(label_message, true);                      /*Enable re-coloring by commands in the text*/
	lv_label_set_align(label_message, LV_LABEL_ALIGN_CENTER);       /*Center aligned lines*/
	lv_label_set_text(label_message, LV_SYMBOL_WARNING"\n#ff0000 SD card not found!!!\n\n");
	
	lv_obj_set_width(label_message, LV_HOR_RES);
	lv_obj_align(label_message, NULL, LV_ALIGN_CENTER, 0, 0);
	
	lv_obj_t * btn = lv_btn_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
	lv_obj_set_event_cb(btn, event_handler_BIT_fault_back_to_home);
	lv_obj_align(btn, label_message, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
	
	lv_obj_t * label_btn_tip = lv_label_create(btn, NULL);
	lv_label_set_text(label_btn_tip, "OK");
}

/*
 *  函数名：   static void event_handler_file_browser_init(lv_obj_t * obj, lv_event_t event)
 *  输入参数： 触发事件的对象
 *  输入参数： 触发的事件类型
 *  函数作用： 打开目录列表事件处理函数
*/
static void event_handler_file_browser_init(lv_obj_t * obj, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
    {
		g_pt_lv_100ask_file_browser->dir_list = lv_list_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
		lv_obj_set_size(g_pt_lv_100ask_file_browser->dir_list, LV_HOR_RES, LV_VER_RES - (LV_VER_RES/9));
		lv_obj_align(g_pt_lv_100ask_file_browser->dir_list, NULL, LV_ALIGN_CENTER, 0, LV_VER_RES/9);	

		g_pt_lv_100ask_file_browser->dir_info = lv_label_create(g_pt_lv_100ask_file_browser->bg_file_browser, NULL);
		lv_obj_set_style_local_text_font(g_pt_lv_100ask_file_browser->dir_info, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_10);  	// 设置文本字体格式
		lv_obj_align(g_pt_lv_100ask_file_browser->dir_info, g_pt_lv_100ask_file_browser->dir_list, LV_ALIGN_OUT_TOP_LEFT, 6, -(lv_obj_get_height(g_pt_lv_100ask_file_browser->dir_info)));

		lv_snprintf(g_pt_lv_100ask_file_browser->dir_path, sizeof(g_pt_lv_100ask_file_browser->dir_path), "S:/");
		lv_100ask_stm32_tool_browser_scan_dir(g_pt_lv_100ask_file_browser->dir_path);
	}
}


/*
 *  函数名：   static void lv_100ask_stm32_tool_browser_scan_dir(char * path)
 *  输入参数： 目录路径
 *  返回值：   无
 *  函数作用： 扫描指定目录
*/
static void lv_100ask_stm32_tool_browser_scan_dir(char * path)
{
	lv_obj_t * btn;
	UINT dir_count = 0;       // 目录统计
	UINT file_count = 0;      // 文件统计

	lv_fs_dir_t dir;
    lv_fs_res_t res;
	
	res = lv_fs_dir_open(&dir, path);
    if(res != LV_FS_RES_OK) 
		printf("OPEN DIR ERROR!\n\r");
	else
	{
		if (lv_list_get_size(g_pt_lv_100ask_file_browser->dir_list) > 0)
			lv_list_clean(g_pt_lv_100ask_file_browser->dir_list);    // 清空列表
		if (strcmp(path, "/") != 0)
		{
			btn = lv_list_add_btn(g_pt_lv_100ask_file_browser->dir_list, LV_SYMBOL_DIRECTORY, ".");  // 添加到列表展示
			btn = lv_list_add_btn(g_pt_lv_100ask_file_browser->dir_list, LV_SYMBOL_DIRECTORY, "..");  // 添加到列表展示
			lv_obj_set_event_cb(btn, event_handler_file_browser_open_dir);   // 分配点击处理事件
		}
	}

    char fn[128];
    while(1) {
		res = lv_fs_dir_read(&dir, fn);
		if(res != LV_FS_RES_OK) {
			printf("READ DIR ERROR!\n\r");
			break;
		}

		/* fn is empty, if not more files to read */
		if(strlen(fn) == 0) {
			break;
		}
		
		if (fn[0] == '\\')  // 以 \ 开头的是文件夹
		{
			dir_count++;
			btn = lv_list_add_btn(g_pt_lv_100ask_file_browser->dir_list, LV_SYMBOL_DIRECTORY, fn);  // 添加到列表展示
		}
		if (fn[0] != '\\')	// 不以 \ 开头的是文件
		{
			file_count++;
			btn = lv_list_add_btn(g_pt_lv_100ask_file_browser->dir_list, LV_SYMBOL_FILE, fn);  // 添加到列表展示
		}
		lv_obj_set_event_cb(btn, event_handler_file_browser_open_dir);   // 分配点击处理事件
    }
	lv_label_set_text_fmt(g_pt_lv_100ask_file_browser->dir_info, "DIR: %d\t\tFILE: %d\nPATH: %s", dir_count, file_count, g_pt_lv_100ask_file_browser->dir_path);  // 展示目录信息
    lv_fs_dir_close(&dir);
}




/*
 *  函数名：   static UINT lv_100ask_stm32_tool_file_browser_strn(const char *p, const char chr)
 *  输入参数： 需要查找的字符串
 *  返回值：   被查找的字符串
 *  函数作用： 计算某个字符串出现的最后位置
*/
static UINT lv_100ask_stm32_tool_file_browser_strn(const char *p, const char chr)
{	
	UINT count = 0,i = 0;
	while(*(p+i))
	{
		if(p[i] == chr)//字符数组存放在一块内存区域中，按索引找字符，指针本身不变
			count = i;
		++i;// 按数组的索引值找到对应指针变量的值
	}
	return count;
}





/*
 *  函数名：   static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数： 触发事件的对象
 *  输入参数： 触发的事件类型
 *  返回值：   无
 *  函数作用： 打开目录事件处理函数
*/
static void event_handler_file_browser_open_dir(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		/* 记录路径 */
		if (g_pt_lv_100ask_file_browser->dir_path != NULL)
		{
			if (strlen(g_pt_lv_100ask_file_browser->dir_path) == 0)
			{
				lv_snprintf(g_pt_lv_100ask_file_browser->dir_path, sizeof(g_pt_lv_100ask_file_browser->dir_path), "S:/");
			}
			else 
			{
				if (strcmp(lv_list_get_btn_text(obj), "..") == 0)
				{
					// 计算某个字符串出现的最后位置
					const char data[1] = "/";
					UINT i = lv_100ask_stm32_tool_file_browser_strn(g_pt_lv_100ask_file_browser->dir_path, *data);

					if (i == 0)
						lv_snprintf(g_pt_lv_100ask_file_browser->dir_path, sizeof(g_pt_lv_100ask_file_browser->dir_path), "S:/");
					else
					{
						// 从下一个字符开始
						i++; 
						for(; g_pt_lv_100ask_file_browser->dir_path[i] != '\0'; i++)
						{
							g_pt_lv_100ask_file_browser->dir_path[i] = '\0';
						}	
					}
				}
				else
				{
					lv_snprintf(g_pt_lv_100ask_file_browser->dir_path, sizeof(g_pt_lv_100ask_file_browser->dir_path), "S:");
					strcat(g_pt_lv_100ask_file_browser->dir_path, lv_list_get_btn_text(obj)); // 拼接字符串
				}
			}		
			lv_100ask_stm32_tool_browser_scan_dir(g_pt_lv_100ask_file_browser->dir_path);
		}
    }
}


/*
 *  函数名：  static void event_handler_BIT_fault_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：自检失败，返回桌面事件处理函数
*/
static void event_handler_BIT_fault_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {	
        if (g_pt_lv_100ask_file_browser->bg_file_browser != NULL)	lv_obj_del(g_pt_lv_100ask_file_browser->bg_file_browser);
		
		/* 释放内存 */
		free(g_pt_lv_100ask_file_browser);

		/* 清空屏幕，返回桌面 */
        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
        lv_100ask_stm32_demo_home(10);
    }
}

/*
 *  函数名：   static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数： 触发事件的对象
 *  输入参数： 触发的事件类型
 *  返回值：   无
 *  函数作用： 返回桌面事件处理函数
*/
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		/* 卸载文件系统 */
		f_mount(NULL, "0:", 0);
		
        if (g_pt_lv_100ask_file_browser->bg_file_browser != NULL)	lv_obj_del(g_pt_lv_100ask_file_browser->bg_file_browser);
		if (g_pt_lv_100ask_file_browser->dir_list != NULL)			lv_obj_del(g_pt_lv_100ask_file_browser->dir_list);
		
		/* 释放内存 */
		free(g_pt_lv_100ask_file_browser);

		/* 清空屏幕，返回桌面 */
        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
        lv_100ask_stm32_demo_home(10);
    }
}
