/**
 ******************************************************************************
 * @file    lv_100ask_stm32_tool_humiture.c
 * @author  百问科技
 * @version V1.2
 * @date    2020-12-12
 * @brief	温湿度检测展示应用
 ******************************************************************************
 * Change Logs:
 * Date           Author          Notes
 * 2020-12-12     zhouyuebiao     First version
 * 2021-01-25     zhouyuebiao     V1.2 
 ******************************************************************************
 * @attention
 *
 * Copyright (C) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 * All rights reserved
 *
 ******************************************************************************
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include <stdlib.h>
#include "lv_100ask_stm32_tool_humiture.h"
#include "stm32f1xx_hal.h"


/**********************
 *  STATIC VARIABLES
 **********************/
static PT_lv_100ask_humiture g_pt_lv_100ask_humiture;  // 结构体


/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_100ask_stm32_tool_humiture_init(void);											// 界面初始化
static int  lv_100ask_dht11_getvalue(uint16_t *pTempture, uint16_t *pHumidity);					// 获取温湿度数据
static void lv_100ask_dht11_in(void);															// 拉高
static void lv_100ask_dht11_gpio_deinit(void);													// 复位引脚
static void lv_100ask_dht11_out(void);															// 拉低
static void lv_100ask_stm32_tool_chart_humidity_anim(lv_obj_t * obj, lv_anim_value_t value);	// 更新绘制温度动画 (使用动画比任务占用的资料更少)
static void lv_100ask_stm32_tool_chart_temperature_anim(lv_obj_t * obj, lv_anim_value_t value);	// 更新绘制湿度动画
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event);						// 返回桌面事件


/*
 *  函数名：   void lv_100ask_stm32_tool_humiture(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 温湿度检测初始化入口
*/
void lv_100ask_stm32_tool_humiture(void)
{
	g_pt_lv_100ask_humiture = (T_lv_100ask_humiture *)malloc(sizeof(T_lv_100ask_humiture));   // 申请内存

	g_pt_lv_100ask_humiture->bg_humiture = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(g_pt_lv_100ask_humiture->bg_humiture, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_y(g_pt_lv_100ask_humiture->bg_humiture, 0);


    lv_100ask_stm32_tool_humiture_init();

    add_title(g_pt_lv_100ask_humiture->bg_humiture, "HUMITURE");
	add_back(g_pt_lv_100ask_humiture->bg_humiture, event_handler_back_to_home);   // 返回桌面按钮
}


/*
 *  函数名：   static void lv_100ask_stm32_tool_humiture_init(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 温湿度检测界面初始化
*/
static void lv_100ask_stm32_tool_humiture_init(void)
{
	lv_anim_t anim_chart_temperature;
    lv_anim_init(&anim_chart_temperature);
	
    // 温度
    /*Create a chart */
    g_pt_lv_100ask_humiture->chart_temperature = lv_chart_create(g_pt_lv_100ask_humiture->bg_humiture, NULL);
    lv_obj_set_style_local_text_font(g_pt_lv_100ask_humiture->chart_temperature, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_10);
    lv_obj_set_style_local_value_str(g_pt_lv_100ask_humiture->chart_temperature, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Temperature: C");
    lv_obj_set_style_local_value_align(g_pt_lv_100ask_humiture->chart_temperature, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, LV_ALIGN_OUT_TOP_LEFT);


    lv_obj_set_click(g_pt_lv_100ask_humiture->chart_temperature, false);
    lv_obj_set_size(g_pt_lv_100ask_humiture->chart_temperature, LV_HOR_RES, (LV_VER_RES / 6 * 2));
    lv_chart_set_div_line_count(g_pt_lv_100ask_humiture->chart_temperature, 4, 0);
    lv_chart_set_point_count(g_pt_lv_100ask_humiture->chart_temperature, LV_100ASK_HUMITURE_POINT_COUNT);   // 设置点数
    lv_chart_set_type(g_pt_lv_100ask_humiture->chart_temperature, LV_CHART_TYPE_LINE);   /*Show lines and points too*/

    lv_obj_set_style_local_pad_left(g_pt_lv_100ask_humiture->chart_temperature,  LV_CHART_PART_BG, LV_STATE_DEFAULT, 4 * (LV_DPI / 11));
    lv_obj_set_style_local_pad_right(g_pt_lv_100ask_humiture->chart_temperature,  LV_CHART_PART_BG, LV_STATE_DEFAULT, 2 * (LV_DPI / 10));
    lv_chart_set_y_tick_length(g_pt_lv_100ask_humiture->chart_temperature, 0, 0);
    lv_chart_set_x_tick_length(g_pt_lv_100ask_humiture->chart_temperature, 0, 0);
    lv_chart_set_y_tick_texts(g_pt_lv_100ask_humiture->chart_temperature, "100\n90\n80\n70\n60\n50\n40\n30\n20\n10\n0", 0, LV_CHART_AXIS_DRAW_LAST_TICK);
    lv_obj_align(g_pt_lv_100ask_humiture->chart_temperature, NULL, LV_ALIGN_IN_TOP_MID, 0, LV_100ASK_HUMITURE_SPACE);


    /*Add a faded are effect*/
    lv_obj_set_style_local_bg_opa(g_pt_lv_100ask_humiture->chart_temperature, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, LV_OPA_50);    /*Max. opa.*/
    lv_obj_set_style_local_bg_grad_dir(g_pt_lv_100ask_humiture->chart_temperature, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
    lv_obj_set_style_local_bg_main_stop(g_pt_lv_100ask_humiture->chart_temperature, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, 255);    /*Max opa on the top*/
    lv_obj_set_style_local_bg_grad_stop(g_pt_lv_100ask_humiture->chart_temperature, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, 0);      /*Transparent on the bottom*/

    /*Add one data series*/
    g_pt_lv_100ask_humiture->ser_temperature = lv_chart_add_series(g_pt_lv_100ask_humiture->chart_temperature, LV_COLOR_RED);
	
	lv_anim_set_var(&anim_chart_temperature, g_pt_lv_100ask_humiture->chart_temperature);
    lv_anim_set_exec_cb(&anim_chart_temperature, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_chart_temperature_anim);
    lv_anim_set_values(&anim_chart_temperature, 0, 1);
    lv_anim_set_time(&anim_chart_temperature, LV_100ASK_HUMITURE_UPDATE_TIME);
    lv_anim_set_repeat_count(&anim_chart_temperature, LV_ANIM_REPEAT_INFINITE);
    lv_anim_start(&anim_chart_temperature);
	

    // 湿度
	lv_anim_t anim_chart_humidity;
    lv_anim_init(&anim_chart_humidity);
	
    /*Create a chart */
    g_pt_lv_100ask_humiture->chart_humidity = lv_chart_create(g_pt_lv_100ask_humiture->bg_humiture, NULL);
    lv_obj_set_style_local_text_font(g_pt_lv_100ask_humiture->chart_humidity, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_10);
    lv_obj_set_style_local_value_str(g_pt_lv_100ask_humiture->chart_humidity, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Humidity: %");
    lv_obj_set_style_local_value_align(g_pt_lv_100ask_humiture->chart_humidity, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, LV_ALIGN_OUT_TOP_LEFT);


    lv_obj_set_click(g_pt_lv_100ask_humiture->chart_humidity, false);
    lv_obj_set_size(g_pt_lv_100ask_humiture->chart_humidity, LV_HOR_RES, (LV_VER_RES / 6 * 2));
    lv_chart_set_div_line_count(g_pt_lv_100ask_humiture->chart_humidity, 4, 0);
    lv_chart_set_point_count(g_pt_lv_100ask_humiture->chart_humidity, LV_100ASK_HUMITURE_POINT_COUNT);   // 设置点数
    lv_chart_set_type(g_pt_lv_100ask_humiture->chart_humidity, LV_CHART_TYPE_LINE);   /*Show lines and points too*/

    lv_obj_set_style_local_pad_left(g_pt_lv_100ask_humiture->chart_humidity,  LV_CHART_PART_BG, LV_STATE_DEFAULT, 4 * (LV_DPI / 11));
    lv_obj_set_style_local_pad_right(g_pt_lv_100ask_humiture->chart_humidity,  LV_CHART_PART_BG, LV_STATE_DEFAULT, 2 * (LV_DPI / 10));
    lv_chart_set_y_tick_length(g_pt_lv_100ask_humiture->chart_humidity, 0, 0);
    lv_chart_set_x_tick_length(g_pt_lv_100ask_humiture->chart_humidity, 0, 0);
    lv_chart_set_y_tick_texts(g_pt_lv_100ask_humiture->chart_humidity, "100\n90\n80\n70\n60\n50\n40\n30\n20\n10\n0", 0, LV_CHART_AXIS_DRAW_LAST_TICK);
    lv_obj_align(g_pt_lv_100ask_humiture->chart_humidity, g_pt_lv_100ask_humiture->chart_temperature, LV_ALIGN_OUT_BOTTOM_MID, 0, 45);


    /*Add a faded are effect*/
    lv_obj_set_style_local_bg_opa(g_pt_lv_100ask_humiture->chart_humidity, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, LV_OPA_50);    /*Max. opa.*/
    lv_obj_set_style_local_bg_grad_dir(g_pt_lv_100ask_humiture->chart_humidity, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
    lv_obj_set_style_local_bg_main_stop(g_pt_lv_100ask_humiture->chart_humidity, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, 255);    /*Max opa on the top*/
    lv_obj_set_style_local_bg_grad_stop(g_pt_lv_100ask_humiture->chart_humidity, LV_CHART_PART_SERIES, LV_STATE_DEFAULT, 0);      /*Transparent on the bottom*/

    /*Add one data series*/
    g_pt_lv_100ask_humiture->ser_humidity = lv_chart_add_series(g_pt_lv_100ask_humiture->chart_humidity, LV_COLOR_NAVY);
	
	lv_anim_set_var(&anim_chart_humidity, g_pt_lv_100ask_humiture->chart_humidity);
    lv_anim_set_exec_cb(&anim_chart_humidity, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_chart_humidity_anim);
    lv_anim_set_values(&anim_chart_humidity, 0, 1);
    lv_anim_set_time(&anim_chart_humidity, LV_100ASK_HUMITURE_UPDATE_TIME);
    lv_anim_set_repeat_count(&anim_chart_humidity, LV_ANIM_REPEAT_INFINITE);
    lv_anim_start(&anim_chart_humidity);
}


/*
 *  函数名：   static void lv_100ask_stm32_tool_chart_temperature_anim(lv_obj_t * obj, lv_anim_value_t value)
 *  输入参数： 动画对象
 *  输入参数： 传递给动画的数据
 *  返回值：   无
 *  函数作用： 更新展示温度
*/
static void lv_100ask_stm32_tool_chart_temperature_anim(lv_obj_t * obj, lv_anim_value_t value)
{
	static bool state = false;
	if (!state)
	{
		state = true;
		static char temperature_str[20] = {0};
		
		/*Set the next points on 'ser_temperature'*/
		lv_snprintf(temperature_str, sizeof(temperature_str), "Temperature: %dC", g_pt_lv_100ask_humiture->mTempture);
		lv_chart_set_next(g_pt_lv_100ask_humiture->chart_temperature, g_pt_lv_100ask_humiture->ser_temperature, g_pt_lv_100ask_humiture->mTempture);
		lv_obj_set_style_local_value_str(g_pt_lv_100ask_humiture->chart_temperature, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, temperature_str);
		
		lv_chart_refresh(g_pt_lv_100ask_humiture->chart_temperature); /*Required after direct set*/
	}
	else
		state = false;

}


 
/*
 *  函数名：   static void lv_100ask_stm32_tool_chart_humidity_anim(lv_obj_t * obj, lv_anim_value_t value)
 *  输入参数： 动画对象
 *  输入参数： 传递给动画的数据
 *  返回值：   无
 *  函数作用： 更新展示湿度
*/
static void lv_100ask_stm32_tool_chart_humidity_anim(lv_obj_t * obj, lv_anim_value_t value)
{
	static bool state = false;
	if (!state)
	{
		state = true;
		static char humidity_str[16] = {0};

		if(lv_100ask_dht11_getvalue(&g_pt_lv_100ask_humiture->mTempture, &g_pt_lv_100ask_humiture->mHumidity) == 1)
		{
			printf("\r\nTempture: %d C", g_pt_lv_100ask_humiture->mTempture);
			printf("\r\nHumidity: %d %", g_pt_lv_100ask_humiture->mHumidity);
		}
		else
		{
			// 本次获取数据失败，使用上一次检测的结果
			g_pt_lv_100ask_humiture->mTempture = 0;
			g_pt_lv_100ask_humiture->mHumidity = 0;
		}
	
		/*Set the next points on 'ser_humidity'*/
		lv_snprintf(humidity_str, sizeof(humidity_str), "Humidity: %d", g_pt_lv_100ask_humiture->mHumidity);
		strcat(humidity_str, "%");
		lv_chart_set_next(g_pt_lv_100ask_humiture->chart_humidity, g_pt_lv_100ask_humiture->ser_humidity, g_pt_lv_100ask_humiture->mHumidity);
		lv_obj_set_style_local_value_str(g_pt_lv_100ask_humiture->chart_humidity, LV_CONT_PART_MAIN, LV_STATE_DEFAULT,  humidity_str);
		
		lv_chart_refresh(g_pt_lv_100ask_humiture->chart_humidity); /*Required after direct set*/

	}
	else
		state = false;
}


/*
 *  函数名：   static void lv_100ask_dht11_gpio_deinit(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： DHT11引脚初始化
*/
static void lv_100ask_dht11_gpio_deinit(void)
{
	LV_100ASK_DHT11_CLK_DIS();
	HAL_GPIO_DeInit(LV_100ASK_DHT11_PORT, LV_100ASK_DHT11_PIN);
}


/*
 *  函数名：   static void lv_100ask_dht11_out(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： DHT11引脚输出
*/
static void lv_100ask_dht11_out(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    lv_100ask_dht11_gpio_deinit();
    
    LV_100ASK_DHT11_CLK_EN();
    
    GPIO_InitStruct.Pin = LV_100ASK_DHT11_PIN;           
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;         
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;   
    HAL_GPIO_Init(LV_100ASK_DHT11_PORT, &GPIO_InitStruct);   
}


/*
 *  函数名：   static void lv_100ask_dht11_in(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： DHT11引脚输入
*/
static void lv_100ask_dht11_in(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    lv_100ask_dht11_gpio_deinit();
    
    LV_100ASK_DHT11_CLK_EN();
    
    GPIO_InitStruct.Pin = LV_100ASK_DHT11_PIN;           
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT; 
    GPIO_InitStruct.Pull = GPIO_PULLUP;    
    HAL_GPIO_Init(LV_100ASK_DHT11_PORT, &GPIO_InitStruct);
}


/*
 *  函数名：   static void lv_100ask_dht11_getvalue(uint16_t *pTempture, uint16_t *pHumidity)
 *  输入参数： 温度
 *  输入参数： 湿度
 *  返回值：   无
 *  函数作用： 获取DHT11检测数据
*/
static int lv_100ask_dht11_getvalue(uint16_t *pTempture, uint16_t *pHumidity)
{
	uint8_t i = 0;
    uint16_t timeout = 100;
    static long long tmp = 0;
    uint16_t tempture_data_inter = 0, tempture_data_dec = 0;
    uint16_t humidity_data_inter = 0, humidity_data_dec = 0;
    uint8_t crc_data = 0;

    tmp = 0;
    // 主机拉低最少18ms
    lv_100ask_dht11_out();
    LV_100ASK_DHT11_O(0);
    LV_100ASK_DHT11_DELAY(1800);
    // 拉高等待DHT11响应，20-40us
    LV_100ASK_DHT11_O(1);
    lv_100ask_dht11_in();
    while((LV_100ASK_DHT11_I==1) && (timeout!=0))
    {
        LV_100ASK_DHT11_DELAY(1);
        timeout--;
    }
    if(timeout==0)
    {
        *pTempture = 0;
        *pHumidity = 0;
		printf("-1\r\n");
        return -1;  // 超时未响应
    }
    
    // 响应DHT11拉低总线80us后再拉高总线80us
    timeout = 100;
    while((LV_100ASK_DHT11_I==0) && (timeout!=0))
    {
        LV_100ASK_DHT11_DELAY(1);
        timeout--;
    }
    if(timeout==0)
    {
        *pTempture = 0;
        *pHumidity = 0;
		printf("-2\r\n");
        return -2;  // 总线响应拉低超时
    }
    
    timeout = 100;
    while((LV_100ASK_DHT11_I==1) && (timeout!=0))
    {
        LV_100ASK_DHT11_DELAY(1);
        timeout--;
    }
    if(timeout==0)
    {
        *pTempture = 0;
        *pHumidity = 0;
		printf("-22\r\n");
        return -2;  // 总线响应拉高超时
    }
    
    // 响应过后获取数据
    for(i=0; i<40; i++)
    {
        timeout = 100;
        while((LV_100ASK_DHT11_I==0) && (timeout!=0))
        {
            LV_100ASK_DHT11_DELAY(1);
            timeout--;
        }
        if(timeout==0)
        {
            *pTempture = 0;
            *pHumidity = 0;
			printf("-222\r\n");
            return -2;  // 总线数据拉低超时
        }
        
        // 等待30us再次读取总线，高表示逻辑1，低表示逻辑0
        LV_100ASK_DHT11_DELAY(3);
        if(LV_100ASK_DHT11_I==1)
        {
            tmp = (tmp<<1) + 1;
            LV_100ASK_DHT11_DELAY(4);    // 再次延时40us补满70us延时等待
        }
        else
        {
            tmp = (tmp<<1) + 0;
        }
    }
    // 数据获取完毕之后，DHT11会拉低总线50us
    timeout = 100;
    while((LV_100ASK_DHT11_I==0) && (timeout!=0))
    {
        LV_100ASK_DHT11_DELAY(1);
        timeout--;
    }
    if(timeout==0)
    {
        *pTempture = 0;
        *pHumidity = 0;
        return -2;  // 总线结束拉低超时
    }
    
    // 主机获取总线权限并拉高延时20ms
    lv_100ask_dht11_out();
    LV_100ASK_DHT11_O(1);
    LV_100ASK_DHT11_DELAY(2000);
    
    // 处理数据
    crc_data = tmp & 0xFF;
    tempture_data_dec = (tmp>>8) & 0xFF;
    tempture_data_inter = (tmp>>16) & 0xFF;
    humidity_data_dec = (tmp>>24) & 0xFF;
    humidity_data_inter = (tmp>>32) & 0xFF;
    
    if(crc_data==(tempture_data_inter + humidity_data_inter + tempture_data_dec + humidity_data_dec))
    {
        *pTempture = tempture_data_inter;
        *pHumidity = humidity_data_inter;
        return 1;
    }
    else
    {
        *pTempture = 0;
        *pHumidity = 0;
        return -1;
    }
}


/*
 *  函数名：  static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：返回桌面事件处理函数
*/
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		/* 删除图表 */
		if (g_pt_lv_100ask_humiture->chart_temperature != NULL)
		{
			lv_anim_del(g_pt_lv_100ask_humiture->chart_temperature, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_chart_temperature_anim);   // 删除动画
			if (g_pt_lv_100ask_humiture->ser_temperature != NULL)	
				lv_chart_clear_serie(g_pt_lv_100ask_humiture->chart_temperature, g_pt_lv_100ask_humiture->ser_temperature); 			// 清空链表
			lv_obj_del(g_pt_lv_100ask_humiture->chart_temperature);
		}
		
		/* 删除图表 */
        if (g_pt_lv_100ask_humiture->ser_humidity != NULL)
		{
			lv_anim_del(g_pt_lv_100ask_humiture->ser_humidity, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_chart_humidity_anim);   // 删除动画
			if (g_pt_lv_100ask_humiture->ser_humidity != NULL)	
				lv_chart_clear_serie(g_pt_lv_100ask_humiture->chart_humidity, g_pt_lv_100ask_humiture->ser_humidity);           // 清空链表
			lv_obj_del(g_pt_lv_100ask_humiture->chart_humidity);
		}
		
		/* 删除背景 */
        if (g_pt_lv_100ask_humiture->bg_humiture != NULL)	
			lv_obj_del(g_pt_lv_100ask_humiture->bg_humiture);
		
		/* 释放内存 */
		free(g_pt_lv_100ask_humiture); 

		/* 清空屏幕，返回桌面 */
        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
        lv_100ask_stm32_demo_home(10);
    }
}

