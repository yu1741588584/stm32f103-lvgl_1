/**
 ******************************************************************************
 * @file    lv_100ask_stm32_music_player.c
 * @author  百问科技
 * @version V1.2
 * @date    2020-12-12
 * @brief	音乐播放器(基于LVGL+VS1053)
 ******************************************************************************
 * Change Logs:
 * Date           Author          Notes
 * 2020-12-12     zhouyuebiao     First version
 * 2021-01-25     zhouyuebiao     V1.2 
 ******************************************************************************
 * @attention
 *
 * Copyright (C) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 * All rights reserved
 *
 ******************************************************************************
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include <stdlib.h>
#include "ff.h"
#include "driver_led.h"
#include "lv_100ask_stm32_music_player.h"
#include "lv_100ask_stm32_vs1053.h"


/**********************
 *  STATIC VARIABLES
 **********************/
static PT_lv_100ask_music_player g_pt_lv_100ask_music_player;  									// 数据结构体
static char g_100ask_stm32_music_dir_path[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH] = {0}; 	// 目录
static char g_100ask_stm32_music_name[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH] = {0};		// 当前播放的文件名称


/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_100ask_stm32_tool_music_player_init(void);								// 音乐播放器初始化入口
static bool lv_100ask_stm32_music_list_init(void);										// 音乐播放器播放列表初始化
static void lv_task_100ask_vs1053_player_song(lv_task_t * task);						// 音乐播放器播放任务
static void lv_100ask_stm32_slider_set_value(lv_obj_t * obj, lv_anim_value_t value);	// 音乐播放器播放任务
static void lv_100ask_stm32_music_BIT_fault(void);										// 自检失败，提示
static void event_handler_BIT_fault_back_to_home(lv_obj_t * obj, lv_event_t event);		// 自检失败，返回桌面处理函数
static void event_handler_select_list_music(lv_obj_t * obj, lv_event_t event);			// 点击播放列表事件处理函数
static void event_handler_close_list_music(lv_obj_t * obj, lv_event_t event);			// 关闭播放列表事件处理函数
static void event_handler_music_player_opt(lv_obj_t * obj, lv_event_t event);			// 事件处理函数，当点击随机播放、上一首、下一首、暂停、播放、打开播放列表得到相应的处理结果
static void event_handler_music_player_slider(lv_obj_t * slider, lv_event_t event);		// 事件处理函数，当进度条(slider)的数值变化是将其同步到(slider_label)，达到显示播放进度条+播放实际时间的效果
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event);				// 返回桌面事件处理函数

	
/*
 *  函数名：  void lv_100ask_stm32_music_player(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：音乐播放器初始化入口
*/
void lv_100ask_stm32_music_player(void)
{
	g_pt_lv_100ask_music_player = (PT_lv_100ask_music_player )malloc(sizeof(PT_lv_100ask_music_player));   // 申请内存
	g_pt_lv_100ask_music_player->player_state = false;
	
	g_pt_lv_100ask_music_player->bg_music_player = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(g_pt_lv_100ask_music_player->bg_music_player, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_y(g_pt_lv_100ask_music_player->bg_music_player, 0);
	
	if (lv_100ask_stm32_music_list_init())
    {
		lv_100ask_stm32_tool_music_player_init();
		lv_obj_move_foreground(g_pt_lv_100ask_music_player->music_list); // 播放列表放到前台
	
	
		g_pt_lv_100ask_music_player->task_handle = lv_task_create(lv_task_100ask_vs1053_player_song, 40, LV_TASK_PRIO_MID, NULL);  // 获取音乐数据任务
		lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);  // 等待用户点击播放
		
		add_title(g_pt_lv_100ask_music_player->bg_music_player, "MUSIC");
		add_back(g_pt_lv_100ask_music_player->bg_music_player, event_handler_back_to_home);   // 返回桌面
	}
}


/*
 *  函数名：  static void lv_100ask_stm32_music_list_init(void)
 *  输入参数：无
 *  返回值：  true-初始化成功 false-初始化失败
 *  函数作用：音乐播放器播放列表初始化
*/
static bool lv_100ask_stm32_music_list_init(void)
{
	g_pt_lv_100ask_music_player->music_list = lv_list_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_size(g_pt_lv_100ask_music_player->music_list, LV_HOR_RES, LV_100ASK_MUSIC_PLAYER_LIST_HIGHT);
	lv_obj_set_style_local_opa_scale(g_pt_lv_100ask_music_player->music_list, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_60); // 设置色彩透明度
	lv_obj_set_y(g_pt_lv_100ask_music_player->music_list, LV_VER_RES);   // 隐藏状态

	FATFS fs;
	FRESULT res_flash;
	
	/* 挂载文件系统 */
	res_flash = f_mount(&fs, "0:", 1);    /* Mount a logical drive */
	if(res_flash == FR_OK)
	{
		printf("FATFS mount succeed\r\n");
		lv_fs_dir_t dir;
		lv_fs_res_t res;
		res = lv_fs_dir_open(&dir, "S:/music");
		if(res != LV_FS_RES_OK) 
		{
			printf("OPEN DIR ERROR!\n\r");
			lv_100ask_stm32_music_BIT_fault();
			return false;
		}
	}
	else if(res_flash == FR_NO_FILESYSTEM)
	{
		printf("SD not inited\r\n");
		lv_100ask_stm32_music_BIT_fault();
		return false;
	}
	else
	{
		printf("FATFS mount Failed\r\n");
		lv_100ask_stm32_music_BIT_fault();
		return false;
	}
	
	lv_obj_t * btn;
	lv_fs_dir_t dir;
	lv_fs_res_t res;

	lv_snprintf(g_100ask_stm32_music_dir_path, sizeof(g_100ask_stm32_music_dir_path), "S:/music");
	res = lv_fs_dir_open(&dir, g_100ask_stm32_music_dir_path);
	lv_snprintf(g_100ask_stm32_music_dir_path, sizeof(g_100ask_stm32_music_dir_path), LV_100ASK_MUSIC_PLAYER_DEFAULT_PATH);
	
	if(res != LV_FS_RES_OK) 
	{
		printf("OPEN DIR ERROR!\n\r");
		//return false;
	}
	else
	{
		if (lv_list_get_size(g_pt_lv_100ask_music_player->music_list) > 0)
			lv_list_clean(g_pt_lv_100ask_music_player->music_list);    // 清空列表
	}

	char fn[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH] = {0};
	while(1) {
		res = lv_fs_dir_read(&dir, fn);
		if(res != LV_FS_RES_OK) {
			printf("READ DIR ERROR!\n\r");
			break;
		}
		
		/* fn is empty, if not more files to read */
		if(strlen(fn) == 0) {
			break;
		}
		
		btn = lv_list_add_btn(g_pt_lv_100ask_music_player->music_list, LV_SYMBOL_AUDIO, fn);  // 添加到列表展示
		lv_btn_set_checkable(btn, true);
		lv_obj_set_event_cb(btn, event_handler_select_list_music);   // 分配点击处理事件
		
		if ((lv_list_get_size(g_pt_lv_100ask_music_player->music_list) > 0) && (strlen(g_100ask_stm32_music_name) <= 0))
		{
			g_pt_lv_100ask_music_player->current_btn = btn;
			//printf("set g_100ask_stm32_music_name:%s\r\n", fn);
			lv_snprintf(g_100ask_stm32_music_name, sizeof(g_100ask_stm32_music_name), fn);
		}
	}
	lv_fs_dir_close(&dir);	
	return true;
}



/*
 *  函数名：  static void lv_100ask_stm32_tool_music_player_init(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：音乐播放器主界面初始化
*/
static void lv_100ask_stm32_tool_music_player_init(void)
{
	volatile lv_fs_res_t res;
	
	char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
	lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
	res = lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);
    if(res != LV_FS_RES_OK) 
	{
		printf("OPEN FILE ERROR!\n\r");
		lv_100ask_stm32_music_BIT_fault();
		return;
	}
	
	VS_Init(); 
	HAL_Delay(100);
	//VS_Sine_Test();	
	VS_HD_Reset();
	VS_Soft_Reset();

	VS_Restart_Play();  					
	VS_Set_All();        							 
	VS_Reset_DecodeTime();
	VS_SPI_SpeedHigh();
	
    // 封面
    LV_IMG_DECLARE(img_lv_100ask_demo_logo);
    lv_obj_t * logo = lv_img_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
    lv_img_set_src(logo, &img_lv_100ask_demo_logo);
    lv_img_set_zoom(logo, 360);
    lv_obj_align(logo, NULL, LV_ALIGN_CENTER, 0, -LV_100ASK_MUSIC_PLAYER_SPACE);

	// 播放/暂停按钮
    g_pt_lv_100ask_music_player->btn_player_ctl = lv_obj_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_style_local_radius(g_pt_lv_100ask_music_player->btn_player_ctl, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 100);                  // 设置圆角
    lv_obj_set_style_local_bg_color(g_pt_lv_100ask_music_player->btn_player_ctl, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);    // 设置颜色
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_player_ctl, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_player_ctl, LV_BTN_PART_MAIN, LV_STATE_PRESSED, LV_COLOR_GRAY);
	lv_obj_set_style_local_value_str(g_pt_lv_100ask_music_player->btn_player_ctl, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_PLAY);
	lv_obj_set_style_local_value_font(g_pt_lv_100ask_music_player->btn_player_ctl, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_22);
    lv_obj_set_size(g_pt_lv_100ask_music_player->btn_player_ctl, LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE, LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE);
    lv_obj_align(g_pt_lv_100ask_music_player->btn_player_ctl, NULL, LV_ALIGN_CENTER, 0, LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET);
    lv_obj_set_event_cb(g_pt_lv_100ask_music_player->btn_player_ctl, event_handler_music_player_opt);

    // 上一首按钮
    g_pt_lv_100ask_music_player->btn_per = lv_obj_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_style_local_radius(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 100);                  	// 设置圆角
    lv_obj_set_style_local_bg_color(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);    	// 设置颜色
	lv_obj_set_style_local_border_opa(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_0);  		// 边框透明度
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GRAY);
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_PRESSED, LV_COLOR_GREEN);
	lv_obj_set_style_local_value_str(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_PREV);
	lv_obj_set_style_local_value_font(g_pt_lv_100ask_music_player->btn_per, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_22);
    lv_obj_set_size(g_pt_lv_100ask_music_player->btn_per, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE);
    lv_obj_align(g_pt_lv_100ask_music_player->btn_per, NULL, LV_ALIGN_CENTER, -(LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE + LV_100ASK_MUSIC_PLAYER_BTN_SPACE), LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET);
    lv_obj_set_event_cb(g_pt_lv_100ask_music_player->btn_per, event_handler_music_player_opt);

    // 随机播放按钮
    g_pt_lv_100ask_music_player->btn_shuffle = lv_obj_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_style_local_radius(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 100);                  	// 设置圆角
    lv_obj_set_style_local_bg_color(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);    	// 设置颜色
	lv_obj_set_style_local_border_opa(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_0);  		// 边框透明度
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GRAY);
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_PRESSED, LV_COLOR_GREEN);
	lv_obj_set_style_local_value_str(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_SHUFFLE);
	lv_obj_set_style_local_value_font(g_pt_lv_100ask_music_player->btn_shuffle, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_22);
    lv_obj_set_size(g_pt_lv_100ask_music_player->btn_shuffle, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE);
    lv_obj_align(g_pt_lv_100ask_music_player->btn_shuffle, NULL, LV_ALIGN_CENTER, -(LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE + LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE + LV_100ASK_MUSIC_PLAYER_BTN_SPACE * 2), LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET);
    lv_obj_set_event_cb(g_pt_lv_100ask_music_player->btn_shuffle, event_handler_music_player_opt);

    // 下一首按钮
    g_pt_lv_100ask_music_player->btn_next = lv_obj_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_style_local_radius(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 100);                  // 设置圆角
    lv_obj_set_style_local_bg_color(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);    	// 设置颜色
	lv_obj_set_style_local_border_opa(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_0);  		// 边框透明度
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GRAY);
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_PRESSED, LV_COLOR_GREEN);
	lv_obj_set_style_local_value_str(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_NEXT);
	lv_obj_set_style_local_value_font(g_pt_lv_100ask_music_player->btn_next, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_22);
    lv_obj_set_size(g_pt_lv_100ask_music_player->btn_next, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE);
    lv_obj_align(g_pt_lv_100ask_music_player->btn_next, NULL, LV_ALIGN_CENTER, (LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE + LV_100ASK_MUSIC_PLAYER_BTN_SPACE), LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET);
    lv_obj_set_event_cb(g_pt_lv_100ask_music_player->btn_next, event_handler_music_player_opt);

    // 播放列表按钮
    g_pt_lv_100ask_music_player->btn_list = lv_obj_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_style_local_radius(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 100);                  // 设置圆角
    lv_obj_set_style_local_bg_color(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);    	// 设置颜色
	lv_obj_set_style_local_border_opa(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_0);  		// 边框透明度
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GRAY);
	lv_obj_set_style_local_value_color(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_PRESSED, LV_COLOR_GREEN);
	lv_obj_set_style_local_value_str(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_LIST);
	lv_obj_set_style_local_value_font(g_pt_lv_100ask_music_player->btn_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_22);
    lv_obj_set_size(g_pt_lv_100ask_music_player->btn_list, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE, LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE);
    lv_obj_align(g_pt_lv_100ask_music_player->btn_list, NULL, LV_ALIGN_CENTER, (LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE + LV_100ASK_MUSIC_PLAYER_OTHER_BTN_SIZE + LV_100ASK_MUSIC_PLAYER_BTN_SPACE * 2), LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET);
    lv_obj_set_event_cb(g_pt_lv_100ask_music_player->btn_list, event_handler_music_player_opt);

    // 播放进度条
    /* Create a slider in the center of the display */
    g_pt_lv_100ask_music_player->slider = lv_slider_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
    lv_obj_set_width(g_pt_lv_100ask_music_player->slider, LV_100ASK_MUSIC_PLAYER_SLIDER_SIZE);
    lv_obj_align(g_pt_lv_100ask_music_player->slider, NULL, LV_ALIGN_CENTER, 0, (LV_100ASK_MUSIC_PLAYER_BTN_Y_OFFSET - LV_100ASK_MUSIC_PLAYER_PLAY_BTN_SIZE));
    lv_obj_set_event_cb(g_pt_lv_100ask_music_player->slider, event_handler_music_player_slider);
    lv_slider_set_range(g_pt_lv_100ask_music_player->slider, 0, 500);
	

    /* Create a label below the slider */
    g_pt_lv_100ask_music_player->slider_label = lv_label_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
    lv_label_set_text(g_pt_lv_100ask_music_player->slider_label, "00:00");
    lv_obj_set_auto_realign(g_pt_lv_100ask_music_player->slider_label, true);
    lv_obj_align(g_pt_lv_100ask_music_player->slider_label, g_pt_lv_100ask_music_player->slider, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, 0);

    // 歌曲标题
    g_pt_lv_100ask_music_player->label_music_name = lv_label_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	//lv_label_set_long_mode(g_pt_lv_100ask_music_player->label_music_name, LV_LABEL_LONG_SROLL_CIRC);     /*Circular scroll*/
	//lv_obj_set_width(g_pt_lv_100ask_music_player->label_music_name, LV_100ASK_MUSIC_PLAYER_SLIDER_SIZE);
	//lv_obj_set_height(g_pt_lv_100ask_music_player->label_music_name, 60);
    lv_obj_set_style_local_text_font(g_pt_lv_100ask_music_player->label_music_name, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_22);  	// text font
    lv_label_set_text(g_pt_lv_100ask_music_player->label_music_name, "100ASK");
    lv_obj_align(g_pt_lv_100ask_music_player->label_music_name, g_pt_lv_100ask_music_player->slider, LV_ALIGN_OUT_TOP_LEFT, 0, -70);

    // 歌曲信息（先使用公司站点信息）
    lv_obj_t * label_info = lv_label_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
    lv_label_set_text(label_info, "lvgl.100ask.net\nwww.100ask.net");
    lv_obj_align(label_info, g_pt_lv_100ask_music_player->label_music_name, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 10);

	// 播放进度条动画
	lv_anim_t a;
	lv_anim_init(&a);
	lv_anim_set_var(&a, g_pt_lv_100ask_music_player->slider);
	lv_anim_set_time(&a, 1000);
	lv_anim_set_delay(&a, 0);
	lv_anim_set_repeat_count(&a, LV_ANIM_REPEAT_INFINITE);
	lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t) lv_100ask_stm32_slider_set_value);
	lv_anim_set_values(&a, 0, 1);
	lv_anim_start(&a);
}


/*
 *  函数名：  static void lv_100ask_stm32_music_BIT_fault(void)
 *  输入参数：无
 *  返回值：  无
 *  函数作用：无sd卡或sd卡路径无效，退出播放器
*/
static void lv_100ask_stm32_music_BIT_fault(void)
{	
	lv_obj_t * label_message = lv_label_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_label_set_long_mode(label_message, LV_LABEL_LONG_BREAK);     /*Break the long lines*/
	lv_label_set_recolor(label_message, true);                      /*Enable re-coloring by commands in the text*/
	lv_label_set_align(label_message, LV_LABEL_ALIGN_CENTER);       /*Center aligned lines*/
	lv_label_set_text(label_message, LV_SYMBOL_WARNING"\n#ff0000 Initialization error!!!\n\n"
							  "Please create the #ff0000 S:/music/# folder in the SD card and place the music files \n");
	lv_obj_set_width(label_message, LV_HOR_RES);
	lv_obj_align(label_message, NULL, LV_ALIGN_CENTER, 0, 0);
	
	lv_obj_t * btn = lv_btn_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
	lv_obj_set_event_cb(btn, event_handler_BIT_fault_back_to_home);
	lv_obj_align(btn, label_message, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
	
	lv_obj_t * label_btn_tip = lv_label_create(btn, NULL);
	lv_label_set_text(label_btn_tip, "OK");
}

/*
 *  函数名：  static void lv_task_100ask_vs1053_player_song(lv_task_t * task)
 *  输入参数：任务描述符
 *  返回值：  无
 *  函数作用：音乐播放器播放任务
*/
static void lv_task_100ask_vs1053_player_song(lv_task_t * task)
{
	uint32_t progress = 0;
	uint32_t read_num;
		
	progress = 0;	
	lv_fs_read(&g_pt_lv_100ask_music_player->file, g_pt_lv_100ask_music_player->buffer, LV_100ASK_MUSIC_PLAYER_BUFSIZE, &read_num);
	g_pt_lv_100ask_music_player->progress += read_num;
	do
	{  	
		if(VS_Send_MusicData(g_pt_lv_100ask_music_player->buffer + progress) == 0)
		{
			LEDRED_TOGGLE;
			progress += 32;
		}
		LEDGREEN_TOGGLE;
	}while(progress < read_num);
	
	LEDGREEN_TOGGLE;
	
	if(read_num != LV_100ASK_MUSIC_PLAYER_BUFSIZE)
	{
		printf("read_num != BUFSIZE!!!\n\r");
		g_pt_lv_100ask_music_player->progress = 0;
		
		lv_obj_t * btn = NULL;
		if (g_pt_lv_100ask_music_player->current_btn != NULL)
			btn = lv_list_get_next_btn(g_pt_lv_100ask_music_player->music_list, g_pt_lv_100ask_music_player->current_btn);
		if (btn != NULL)
		{
			g_pt_lv_100ask_music_player->current_btn = btn;
			
			lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);  // 暂停播放
			lv_fs_close(&g_pt_lv_100ask_music_player->file);
			lv_snprintf(g_100ask_stm32_music_name, sizeof(g_100ask_stm32_music_name), "%s", lv_list_get_btn_text(g_pt_lv_100ask_music_player->current_btn));
			lv_label_set_text(g_pt_lv_100ask_music_player->label_music_name, g_100ask_stm32_music_name);
			
			char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
			lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
			lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);

			VS_Restart_Play();  					
			lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 播放
			g_pt_lv_100ask_music_player->player_state = true;
		}
		else
		{
			//VS_HD_Reset();
			//VS_Soft_Reset();

			//VS_Restart_Play();  					
			//VS_Set_All();        							 
			//VS_Reset_DecodeTime();
			//VS_SPI_SpeedHigh();
			lv_fs_seek(&g_pt_lv_100ask_music_player->file, 0);
			progress = 0;	
		}
	}	
}   



/*
 *  函数名：   static void lv_100ask_stm32_slider_set_value(lv_obj_t * obj, lv_anim_value_t value)
 *  输入参数： 动画对象
 *  输入参数： 传递给动画的数据
 *  返回值：   无
 *  函数作用： 音乐播放器播放任务
*/
static void lv_100ask_stm32_slider_set_value(lv_obj_t * obj, lv_anim_value_t value)
{
	static bool state = false;
	if (state)
	{
		uint16_t play_time_second = 0;
		play_time_second = VS_Get_DecodeTime();
		if (play_time_second > 0)
		{

			char tmpc[16] = {0}; // 保持 00:00:00 的格式
			if (play_time_second >= 60)
				strcat(tmpc, (play_time_second % 3600 / 60)  >=10 ? "%d:":"0%d:");		// 分
			else
				strcat(tmpc, "00:");		// 分
			strcat(tmpc, (play_time_second % 60)     	 >=10 ? "%d":"0%d");		// 秒
			if (play_time_second >= 60)
				lv_label_set_text_fmt(g_pt_lv_100ask_music_player->slider_label, tmpc, (play_time_second % 3600 / 60), (play_time_second % 60));
			else
				lv_label_set_text_fmt(g_pt_lv_100ask_music_player->slider_label, tmpc, (play_time_second % 60));
			lv_slider_set_value(g_pt_lv_100ask_music_player->slider, play_time_second, LV_ANIM_OFF);
		}
		state = false;
	}
	else
		state = true;
}

#if 0
/*
 *  函数名：  static void lv_100ask_str_end_with(const char *str1, char *str2)
 *  输入参数：str
 *  输入参数：str
 *  返回值：  是返回1，不是返回0，出错返回-1
 *  函数作用：判断str1是否以str2结尾
*/
static int lv_100ask_str_end_with(const char *str1, char *str2)
{
    if(str1 == NULL || str2 == NULL)
        return -1;
    int len1 = strlen(str1);
    int len2 = strlen(str2);
    if((len1 < len2) ||  (len1 == 0 || len2 == 0))
        return -1;
    while(len2 >= 1)
    {
        if(str2[len2 - 1] != str1[len1 - 1])
            return 0;
        len2--;
        len1--;
    }
    return 1;
}
#endif



/*
 *  函数名：  static void lv_task_100ask_vs1053_player_song(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象(这里是进度条对象)
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：当进度条(slider)的数值变化是将其同步到(slider_label)，达到显示播放进度条+播放实际时间的效果
*/
static void event_handler_music_player_slider(lv_obj_t * slider, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED)
    {
        char buf[8]; /* max 3 bytes for number plus 1 null terminating byte */
		lv_snprintf(buf, sizeof(buf), "0%u:00", lv_slider_get_value(slider));
        
        lv_label_set_text(g_pt_lv_100ask_music_player->slider_label, buf);
    }
}



/*
 *  函数名：  static void event_handler_music_player_opt(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象(包括了随机播放、上一首、下一首、暂停、播放、打开播放列表)
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：当点击随机播放、上一首、下一首、暂停、播放、打开播放列表得到相应的处理结果
*/
static void event_handler_music_player_opt(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		const void * data = lv_event_get_data();  // 获取当前事件的'data'参数
        if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), LV_SYMBOL_PLAY) == 0)
        {
			if(data != NULL)
			{
				printf("LV_SYMBOL_PLAYdata != NULL! data is: %s\r\n", (char *)data);
				g_pt_lv_100ask_music_player->player_state = true;
				lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_PAUSE);
				lv_label_set_text_fmt(g_pt_lv_100ask_music_player->label_music_name, "%s", g_100ask_stm32_music_name);
			}
			else
			{
				g_pt_lv_100ask_music_player->player_state = true;
				lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_PAUSE);
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 播放
				lv_label_set_text_fmt(g_pt_lv_100ask_music_player->label_music_name, "%s", g_100ask_stm32_music_name);
			}
        }
        else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), LV_SYMBOL_PAUSE) == 0)
        {
			if(data == NULL)
			{
				g_pt_lv_100ask_music_player->player_state = false;
				lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_PLAY);
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);  // 暂停    
			}
			else			
			{
				g_pt_lv_100ask_music_player->player_state = true;
				lv_obj_set_style_local_value_str(obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_PAUSE);
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 播放
				lv_label_set_text_fmt(g_pt_lv_100ask_music_player->label_music_name, "%s", g_100ask_stm32_music_name);
			}
        }
		else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), LV_SYMBOL_PREV) == 0)  // 上一首
        {
			lv_obj_t * btn = NULL;
			if (g_pt_lv_100ask_music_player->current_btn != NULL)
				btn = lv_list_get_prev_btn(g_pt_lv_100ask_music_player->music_list, g_pt_lv_100ask_music_player->current_btn);
			if (btn != NULL)
			{
				g_pt_lv_100ask_music_player->current_btn = btn;
				
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);  // 暂停播放
				lv_fs_close(&g_pt_lv_100ask_music_player->file);
				lv_snprintf(g_100ask_stm32_music_name, sizeof(g_100ask_stm32_music_name), "%s", lv_list_get_btn_text(g_pt_lv_100ask_music_player->current_btn));
				lv_label_set_text(g_pt_lv_100ask_music_player->label_music_name, g_100ask_stm32_music_name);
				
				char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
				lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
				lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);

				VS_Restart_Play();  					     							 
				VS_Reset_DecodeTime();
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 播放
				lv_event_send_func(event_handler_music_player_opt, g_pt_lv_100ask_music_player->btn_player_ctl, LV_EVENT_CLICKED, g_100ask_stm32_music_name);  // 通知按钮
				g_pt_lv_100ask_music_player->player_state = true;
			}
        }
		else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), LV_SYMBOL_NEXT) == 0)  // 下一首
        {
			lv_obj_t * btn = NULL;
			if (g_pt_lv_100ask_music_player->current_btn != NULL)
				btn = lv_list_get_next_btn(g_pt_lv_100ask_music_player->music_list, g_pt_lv_100ask_music_player->current_btn);
			if (btn != NULL)
			{
				g_pt_lv_100ask_music_player->current_btn = btn;
				
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);  // 暂停播放
				lv_fs_close(&g_pt_lv_100ask_music_player->file);
				lv_snprintf(g_100ask_stm32_music_name, sizeof(g_100ask_stm32_music_name), "%s", lv_list_get_btn_text(g_pt_lv_100ask_music_player->current_btn));
				lv_label_set_text(g_pt_lv_100ask_music_player->label_music_name, g_100ask_stm32_music_name);
				
				char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
				lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
				lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);

				VS_Restart_Play();  					      							 
				VS_Reset_DecodeTime();
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 播放
				lv_event_send_func(event_handler_music_player_opt, g_pt_lv_100ask_music_player->btn_player_ctl, LV_EVENT_CLICKED, g_100ask_stm32_music_name);  // 通知按钮
				g_pt_lv_100ask_music_player->player_state = true;
			}
        }
		else if (strcmp(lv_obj_get_style_value_str(obj, LV_BTN_PART_MAIN), LV_SYMBOL_LIST) == 0)
        {
			lv_obj_t * btn;
			lv_fs_dir_t dir;
			lv_fs_res_t res;

			if (g_pt_lv_100ask_music_player->player_state)
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);
			lv_fs_close(&g_pt_lv_100ask_music_player->file);

			res = lv_fs_dir_open(&dir, g_100ask_stm32_music_dir_path);
			if(res != LV_FS_RES_OK) 
				printf("OPEN DIR ERROR!\n\r");
			else
			{
				if (lv_list_get_size(g_pt_lv_100ask_music_player->music_list) > 0)
					lv_list_clean(g_pt_lv_100ask_music_player->music_list);    // 清空列表
			}

			char fn[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
			while(1) {
				res = lv_fs_dir_read(&dir, fn);
				if(res != LV_FS_RES_OK) {
					printf("READ DIR ERROR!\n\r");
					break;
				}

				/* fn is empty, if not more files to read */
				if(strlen(fn) == 0) {
					break;
				}
				
				btn = lv_list_add_btn(g_pt_lv_100ask_music_player->music_list, LV_SYMBOL_AUDIO, fn);  // 添加到列表展示
				lv_btn_set_checkable(btn, true);
				lv_obj_set_event_cb(btn, event_handler_select_list_music);   // 分配点击处理事件
			}
			lv_fs_dir_close(&dir);
			
			char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
			lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
			lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);
			lv_fs_seek(&g_pt_lv_100ask_music_player->file, g_pt_lv_100ask_music_player->progress);
			
			if (g_pt_lv_100ask_music_player->player_state)
				lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 继续播放

			lv_obj_t * btn_close_list = lv_btn_create(g_pt_lv_100ask_music_player->bg_music_player, NULL);
			lv_obj_set_style_local_radius(btn_close_list, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 0); // 设置圆角
			lv_obj_set_size(btn_close_list, LV_HOR_RES, 40);
            lv_obj_set_event_cb(btn_close_list, event_handler_close_list_music);  // 关联到关闭播放列表动画事件
            lv_obj_align(btn_close_list, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
            lv_obj_t * label = lv_label_create(btn_close_list, NULL);
            lv_label_set_text(label, "CLOSE");
			
			// 打开播放列表动画
			lv_anim_t a;
			lv_anim_init(&a);
			lv_anim_set_var(&a, g_pt_lv_100ask_music_player->music_list);
			lv_anim_set_time(&a, LV_100ASK_MUSIC_PLAYER_LIST_ANIM_Y);
			lv_anim_set_delay(&a, 0);
			lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t) lv_obj_set_y);
			lv_anim_set_values(&a, lv_obj_get_y(g_pt_lv_100ask_music_player->music_list), (LV_100ASK_MUSIC_PLAYER_LIST_Y_OFFSET - lv_obj_get_height(btn_close_list)));
			lv_anim_start(&a);				
        }
    }
}


/*
 *  函数名：  static void event_handler_close_list_music(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象(这里是关闭播放列表对象)
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：关闭播放列表动画处理事件
*/
static void event_handler_close_list_music(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        // 动画
		lv_anim_t a;
		lv_anim_init(&a);
		lv_anim_set_var(&a, g_pt_lv_100ask_music_player->music_list);
		lv_anim_set_time(&a, LV_100ASK_MUSIC_PLAYER_LIST_ANIM_Y);
		lv_anim_set_delay(&a, 0);
		lv_anim_set_exec_cb(&a, (lv_anim_exec_xcb_t) lv_obj_set_y);
		lv_anim_set_values(&a, lv_obj_get_y(g_pt_lv_100ask_music_player->music_list), LV_VER_RES);
		lv_anim_start(&a);
		lv_obj_del(obj);
    }
}


/*
 *  函数名：  static void event_handler_select_list_music(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象(这里是打开播放列表对象)
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：点击播放列表处理事件
*/
static void event_handler_select_list_music(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        //printf("Clicked: %s\n\r", lv_list_get_btn_text(obj));
		// 如果正在播放
		if (g_pt_lv_100ask_music_player->player_state)
		{
			lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_OFF);  // 暂停
			
			lv_fs_close(&g_pt_lv_100ask_music_player->file);
			lv_snprintf(g_100ask_stm32_music_name, sizeof(g_100ask_stm32_music_name), "%s", lv_list_get_btn_text(obj));
			lv_label_set_text(g_pt_lv_100ask_music_player->label_music_name, g_100ask_stm32_music_name);
			char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
			lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
			lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);//
		}
		else
		{
			lv_fs_close(&g_pt_lv_100ask_music_player->file);
			lv_snprintf(g_100ask_stm32_music_name, sizeof(g_100ask_stm32_music_name), "%s", lv_list_get_btn_text(obj));
			
			char tmp_str[LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH];
			lv_snprintf(tmp_str, sizeof(tmp_str), "%s%s", g_100ask_stm32_music_dir_path, g_100ask_stm32_music_name);
			lv_fs_open(&g_pt_lv_100ask_music_player->file, tmp_str, LV_FS_MODE_RD);
			g_pt_lv_100ask_music_player->player_state = true;
		}
	
		VS_Restart_Play();  					        							 
		VS_Reset_DecodeTime();
		g_pt_lv_100ask_music_player->current_btn = obj;  // 指向当前的播放文件的指针(对于list)
		lv_task_set_prio(g_pt_lv_100ask_music_player->task_handle, LV_TASK_PRIO_MID);  // 播放
		lv_event_send_func(event_handler_music_player_opt, g_pt_lv_100ask_music_player->btn_player_ctl, LV_EVENT_CLICKED, g_100ask_stm32_music_name);  // 通知按钮
    }
}


/*
 *  函数名：  static void event_handler_BIT_fault_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：自检失败，返回桌面事件处理函数
*/
static void event_handler_BIT_fault_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		if (g_pt_lv_100ask_music_player->bg_music_player != NULL)		
		lv_obj_del(g_pt_lv_100ask_music_player->bg_music_player);		// 清空背景

		memset(g_100ask_stm32_music_name, 0, sizeof(char) * LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH);  // 重置文件名

		/* 释放内存 */
		free(g_pt_lv_100ask_music_player);

		/* 清空屏幕，返回桌面 */
		lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
		lv_100ask_stm32_demo_home(10);
	}
}
	
	
/*
 *  函数名：  static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数：触发事件的对象
 *  输入参数：触发的事件类型
 *  返回值：  无
 *  函数作用：返回桌面事件处理函数
*/
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		// 清除空间
		if (g_pt_lv_100ask_music_player->task_handle != NULL)			lv_task_del(g_pt_lv_100ask_music_player->task_handle);  		// 删除任务
		if (g_pt_lv_100ask_music_player->current_btn != NULL)       	lv_obj_del(g_pt_lv_100ask_music_player->current_btn);  			// 播放的文件名
		if (g_pt_lv_100ask_music_player->label_music_name != NULL)      lv_obj_del(g_pt_lv_100ask_music_player->label_music_name);  	// 播放的文件名
		if (g_pt_lv_100ask_music_player->btn_player_ctl != NULL)       	lv_obj_del(g_pt_lv_100ask_music_player->btn_player_ctl);  		// 暂停、播放按钮
		if (g_pt_lv_100ask_music_player->btn_per != NULL)       		lv_obj_del(g_pt_lv_100ask_music_player->btn_per);  				// 上一首按钮
		if (g_pt_lv_100ask_music_player->btn_next != NULL)       		lv_obj_del(g_pt_lv_100ask_music_player->btn_next);  			// 下一首按钮
		if (g_pt_lv_100ask_music_player->btn_shuffle != NULL)       	lv_obj_del(g_pt_lv_100ask_music_player->btn_shuffle);  			// 随机播放按钮
		if (g_pt_lv_100ask_music_player->btn_list != NULL)       		lv_obj_del(g_pt_lv_100ask_music_player->btn_list);  			// 播放列表按钮
		if (g_pt_lv_100ask_music_player->slider != NULL)       			lv_obj_del(g_pt_lv_100ask_music_player->slider);  				// 播放进度条
        if (g_pt_lv_100ask_music_player->slider_label != NULL)       	lv_obj_del(g_pt_lv_100ask_music_player->slider_label);  		// 播放时长
        if (g_pt_lv_100ask_music_player->music_list != NULL)			lv_obj_del(g_pt_lv_100ask_music_player->music_list);    		// 清理播放列表
		if (g_pt_lv_100ask_music_player->bg_music_player != NULL)		lv_obj_del(g_pt_lv_100ask_music_player->bg_music_player);		// 清空背景
	
		/* 熄灭LED灯 */
		LEDRED_OFF;
		LEDGREEN_OFF;
		LEDBLUE_OFF;
		
		/* 关闭文件句柄 */
		lv_fs_close(&g_pt_lv_100ask_music_player->file);
		f_mount(NULL, "0:", 0);
		memset(g_100ask_stm32_music_name, 0, sizeof(char) * LV_100ASK_MUSIC_PLAYER_MUSIC_NAME_MAX_LENGTH);  // 重置文件名
		
		/* 释放内存 */
		free(g_pt_lv_100ask_music_player);
		
		/* 清空屏幕，返回桌面 */
        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
        lv_100ask_stm32_demo_home(10);
    }
}
